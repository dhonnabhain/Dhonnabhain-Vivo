package com.dhonnabhain.donovan.vivo;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import android.util.Log;
import java.util.ArrayList;

import com.dhonnabhain.donovan.vivo.webservice.Album;
import com.dhonnabhain.donovan.vivo.webservice.Artist;
import com.dhonnabhain.donovan.vivo.webservice.Song;
import com.dhonnabhain.donovan.vivo.webservice.WebService;
import com.squareup.picasso.Picasso;

public class AlbumActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        final Bundle datas = getIntent().getExtras();
        final AlbumActivity self = this;

        String artistName = datas.getString("artistName");
        String albumIndex = datas.getString("albumIndex");
        String albumName = datas.getString("albumName");
        String artistID = datas.getString("artistID");

        // Modification du titre de la vue
        TextView topLabel = (TextView) findViewById(R.id.topLabel);
        topLabel.setText(datas.getString("artistName"));

        /// Éouteur pour le bouton back
        ImageView backBut = (ImageView)findViewById(R.id.backBut);
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                self.finish();
            }
        });

        createElements(artistName, albumIndex, albumName, artistID);

    }

    public void createElements(final String artistName, String albumIndex, final String albumName, String artistID){


        // Nom de l'album
        TextView albumNameV = (TextView) findViewById(R.id.albumName);
        albumNameV.setText(albumName);

        // Récupération des informations de l'album
        int artistInd = Integer.parseInt(artistID);
        final int albumInd = Integer.parseInt(albumIndex);

        ArrayList<Artist> listeArtistes = WebService.getAll();
        final Artist oneArtist = listeArtistes.get(artistInd);

        final Album oneAlbum = oneArtist.albums.get(albumInd);

        final AlbumActivity self = this;

        // Cover
        ImageView cover = (ImageView)findViewById(R.id.albumCover);
        Picasso.with(this.getApplicationContext()).load(oneAlbum.cover).into(cover);

        // Infos
        TextView release = (TextView)findViewById(R.id.albumRelease);
        release.setText("Sorti le " + oneAlbum.releaseDate);

        TextView label = (TextView)findViewById(R.id.albumLabel);
        label.setText(oneAlbum.label);

        // Titles
        ListView albumSongs = (ListView)findViewById(R.id.songsTitle);
        ArrayAdapter<Song> titlesAdapter = new ArrayAdapter<Song>(this, R.layout.title, oneAlbum.songs);
        albumSongs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Titre: ", parent.getItemAtPosition(position).toString());
                Log.i("File: ", oneAlbum.songs.get(position).file);

                Intent intent = new Intent(self, PlayerActivity.class);
                    intent.putExtra("artistName", artistName);
                    intent.putExtra("albumIndex", albumInd);
                    intent.putExtra("song", position);
                    startActivity(intent);
            }
        });

        // Comments
        LinearLayout commentScroll = (LinearLayout) findViewById(R.id.albumComments);
        for (int c = 0; c < oneAlbum.comments.size(); c++) {
            View albumCommentView = getLayoutInflater().inflate(R.layout.comment, null);

            // Nom du "magazine"
            TextView commentName = (TextView) albumCommentView.findViewById(R.id.commentName);
            commentName.setText(oneAlbum.comments.get(c).name);

            // Contenu
            TextView commentContent = (TextView) albumCommentView.findViewById(R.id.commentContent);
            commentContent.setText(oneAlbum.comments.get(c).content);

            // Ajout dans la scrollView
            commentScroll.addView(albumCommentView);
        }

        albumSongs.setAdapter(titlesAdapter);
    }
}
