package com.dhonnabhain.donovan.vivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

// Internal
import com.dhonnabhain.donovan.vivo.webservice.WebService;
import com.dhonnabhain.donovan.vivo.webservice.WebServiceListener;


public class MainActivity extends AppCompatActivity implements WebServiceListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebService serv = new WebService(this);
        serv.loadData();
    }

    public void onError(String message){
        String errorM = "Impossible de charger les données";

        TextView field = new TextView(this);
        field.setText(errorM);
    }

    public void onSuccess(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}
