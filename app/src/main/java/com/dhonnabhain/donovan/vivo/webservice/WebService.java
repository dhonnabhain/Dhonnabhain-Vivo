package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 08/03/2017.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebService {
    public static Data data = new Data();

    // Définition de la base de l'URL
    private final String webserBase = "http://vivo.lastcrsml.xyz/api/datas/";

    private WebServiceListener listener = null;

    public WebService(WebServiceListener listener) {
        this.listener = listener;
    }

    // Chargement des données
    public void loadData() {
        WebService.data.reset();

        loadArtists();
    }

    // Retourne la liste de tous les artistes
    public static ArrayList<Artist> getAll() {
        return data.artists;
    }

    public static Album getAlbum(int albumId) {
        for (Artist a : getAll()) {
            for (Album al : a.albums) {
                if (al.id == albumId)
                    return al;
            }
        }

        return null;
    }

    //
    private void loadArtists() {
        new JsonTask().execute("getAll");
    }

    private void parseArtists(String jsonData) throws JSONException {
        JSONArray artists = new JSONArray(jsonData);

        for (int i = 0; i < artists.length(); i++) {
            JSONObject jsonArtist = artists.getJSONObject(i);

            Artist artist = new Artist();
            artist.name = jsonArtist.getString("name");
            artist.country = jsonArtist.getString("country");

            JSONArray jsonAlbums = jsonArtist.getJSONArray("albums");

            for (int j = 0; j < jsonAlbums.length(); j++) {
                JSONObject jsonAlbum = jsonAlbums.getJSONObject(j);

                Album album = new Album();
                album.name = jsonAlbum.getString("name");
                album.cover = jsonAlbum.getString("cover");
                album.releaseDate = jsonAlbum.getString("release");
                album.label = jsonAlbum.getString("label");
                album.style = jsonAlbum.getString("style");

                JSONArray jsonSongs = jsonAlbum.getJSONArray("songs");

                for (int k = 0; k < jsonSongs.length(); k++) {
                    JSONObject jsonTrack = jsonSongs.getJSONObject(k);

                    Song song = new Song();
                    song.title = jsonTrack.getString("title");
                    song.file = jsonTrack.getString("file");
                    song.duration = jsonTrack.getString("duration");

                    album.songs.add(song);
                }

                JSONArray jsonComments = jsonAlbum.getJSONArray("comments");

                for (int l = 0; l < jsonComments.length(); l++) {
                    JSONObject jsonComment = jsonComments.getJSONObject(l);

                    Comment comment = new Comment();
                    comment.name = jsonComment.getString("name");
                    comment.content = jsonComment.getString("content");
                    comment.author = jsonComment.getString("author");

                    album.comments.add(comment);
                }

                artist.albums.add(album);
            }

            JSONArray jsonImages = jsonArtist.getJSONArray("images");

            for (int m = 0; m < jsonImages.length(); m++) {
                JSONObject jsonImage = jsonImages.getJSONObject(m);

                Image image = new Image();
                image.link = jsonImage.getString("link");

                artist.images.add(image);
            }

            JSONArray jsonConcerts = jsonArtist.getJSONArray("concerts");

            for (int n = 0; n < jsonConcerts.length(); n++) {
                JSONObject jsonConcert = jsonConcerts.getJSONObject(n);

                Concert concert = new Concert();
                concert.date = jsonConcert.getString("date");
                concert.city = jsonConcert.getString("city");
                concert.place = jsonConcert.getString("place");

                artist.concerts.add(concert);
            }

            WebService.data.artists.add(artist);
        }

        allDataParsed();
    }

    private void allDataParsed() {
        WebService.data.loaded = true;
        listener.onSuccess();
    }

    private class JsonTask extends AsyncTask<String, String, String> {
        private String request;
        private String service;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... params) {
            //this.request = params[0];

            // Extraction de la requete
            Pattern pattern = Pattern.compile("[A-Z][a-z]{2,}");
            Matcher matcher = pattern.matcher(params[0]);

            while (matcher.find()) {
                //Log.d("Regex: ", matcher.group().toLowerCase());
                this.request = matcher.group().toLowerCase();
            }

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(webserBase + request);
                Log.d("URL: ", url.toString());
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                /*if (this.request.equals("getArtists"))
                    parseArtists(result);*/
                if (this.request.equals("all"))
                    parseArtists(result);
                /*else if (this.request.equals("getConcerts"))
                    parseConcerts(result);
                else if (this.request.equals("getPlaylistByUser/" + DRUPAL_USER_ID))
                    parsePlaylists(result);*/
            } catch (JSONException e) {
                e.printStackTrace();
                listener.onError(e.getMessage());
            }
        }
    }
}
