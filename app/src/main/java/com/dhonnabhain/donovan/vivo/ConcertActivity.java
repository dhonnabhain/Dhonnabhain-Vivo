package com.dhonnabhain.donovan.vivo;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhonnabhain.donovan.vivo.webservice.Artist;
import com.dhonnabhain.donovan.vivo.webservice.Concert;
import com.dhonnabhain.donovan.vivo.webservice.WebService;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;

public class ConcertActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concert);

        final Bundle datas = getIntent().getExtras();
        Integer concertId = datas.getInt("concertID");
        Integer artistId = datas.getInt("artistID");

        final ConcertActivity self = this;
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        // Modification du titre de la vue
        TextView topLabel = (TextView) findViewById(R.id.topLabel);
        topLabel.setText(datas.getString("artistName"));

        // Écouteur pour le bouton back
        ImageView backBut = (ImageView)findViewById(R.id.backBut);
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                self.finish();
            }
        });

        // Changement des texts
        createElements(artistId, concertId);

    }

    private void createElements(Integer artistID, Integer concertID){
        ArrayList<Artist> listeArtistes = WebService.getAll();

        Artist oneArtist = listeArtistes.get(artistID);
        Concert oneConcert = oneArtist.concerts.get(concertID);

        // Récupération des éléments présents
        TextView place = (TextView)findViewById(R.id.concertPlace);
        TextView address = (TextView)findViewById(R.id.concertAdress);
        TextView date = (TextView)findViewById(R.id.concertDate);

        // Modification des textes
        place.setText(oneConcert.place);
        address.setText(oneConcert.city);
        date.setText(oneConcert.date);

        // Map
        initMap();
    }

    private void initMap(){
        MapView map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(9);
        GeoPoint startPoint = new GeoPoint(48.8583, 2.2944);
        mapController.setCenter(startPoint);
    }

    public void onResume(){
        super.onResume();
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
    }
}
