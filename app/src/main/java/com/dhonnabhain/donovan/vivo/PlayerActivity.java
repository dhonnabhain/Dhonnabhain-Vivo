package com.dhonnabhain.donovan.vivo;

import android.media.AudioManager;
import android.media.MediaPlayer;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import com.dhonnabhain.donovan.vivo.webservice.Album;
import com.dhonnabhain.donovan.vivo.webservice.Artist;
import com.dhonnabhain.donovan.vivo.webservice.MediaControllerAlwaysVisible;
import com.dhonnabhain.donovan.vivo.webservice.Song;
import com.dhonnabhain.donovan.vivo.webservice.WebService;
import com.squareup.picasso.Picasso;

public class PlayerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener, MediaController.MediaPlayerControl, MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnCompletionListener {

    MediaPlayer player = new MediaPlayer();
    MediaController mediaController;

    Album album;
    Integer song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        final Bundle datas = getIntent().getExtras();
        final PlayerActivity self = this;

        Integer albumID = datas.getInt("albumIndex");

        // Modification du titre de la vue
        TextView topLabel = (TextView) findViewById(R.id.topLabel);
        topLabel.setText(datas.getString("artistName"));

        // Éouteur pour le bouton back
        ImageView backBut = (ImageView)findViewById(R.id.backBut);
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                self.finish();
            }
        });

        song = datas.getInt("song");
        album = WebService.getAlbum(albumID);

        // Ajout de la cover
        ImageView cover = (ImageView)findViewById(R.id.playerCover);
        Picasso.with(this.getApplicationContext()).load(album.cover).into(cover);

        // Titre + album
        TextView title = (TextView)findViewById(R.id.songName);
        Log.i("bordel: ", Integer.toString(song));
        title.setText(album.songs.get(song).title);

        TextView albumName = (TextView)findViewById(R.id.albumName);
        albumName.setText(album.name);

        mediaController = new MediaControllerAlwaysVisible(this);

        try {
            Log.i("File: ", album.songs.get(song).file);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(album.songs.get(song).file);
            player.prepareAsync();
            player.setOnPreparedListener(this);

            //player.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.start();
        player.setOnCompletionListener(this);
    }

    public void onAttachedToWindow(){
        mediaController.setAnchorView(findViewById(R.id.mediaController));
        mediaController.setMediaPlayer(this);
        mediaController.show();
    }

    @Override
    public void start() {
        player.start();
    }

    @Override
    public void pause() {
        player.pause();
    }

    @Override
    public int getDuration() {
        return player.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    @Override
    public void seekTo(int pos) {
        Log.i("Seek: ", Integer.toString(pos));
        player.seekTo(pos);
    }

    @Override
    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        //int percentage = (player.getCurrentPosition() * 100) / player.getDuration();

        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {


    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        song++;
        TextView title = (TextView)findViewById(R.id.songTitle);
        title.setText(album.songs.get(song).title);
        try{
            player.reset();
            player.setDataSource(album.songs.get(song).file);
            player.prepare();
            player.setOnPreparedListener(this);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
